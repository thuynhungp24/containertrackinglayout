<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Tracking</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Lato|Varela+Round&display=swap" rel="stylesheet">
    <script type="text/javascript" src="theme/js/custom.js"></script>
    <style>

        .tracking-table {
            overflow-x: hidden;
            margin-bottom: -17px;
        }

        table {
            border-collapse: collapse;
            border-radius: 10px;
            font-size: 13px;
            font-family: 'Varela Round', 'Lato', sans-serif;
            color: #a6a6a6;
            margin: 0;
            padding: 0;
        }
        
        th, td {
            padding: 5px 5px 5px 15px;
            text-align: left;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }
        
        th {
            position: sticky;
            top: 0;
            /* background-image: linear-gradient(to bottom, #8080ff, #3333ff); */
            background-color: #2A5992;
            color: #ffffff;
            font-size: 20px;
        }
        
        tr td:nth-child(2) {
            color: #000000;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        tr:hover {
            background-color: #ccccff;
        }

        tr {
            padding-left: 25px;
            padding-right: 25px;
        }

        @media screen and (max-width: 450px) {

            table {
                font-size: 16px;
                border: 0;
            }

            table, thead, tbody, th, tr, td {
                display: block;
            }

            thead tr {
                position: absolute;
                top: -9999px;
                left: -9999px;
                border: 0;
            }

            td {
                border-bottom: 1px solid #ccc;
                position: relative;
                padding-left: 25%;
            }

            td:before {
                position: absolute;
                top: 10px;
                left: 6px;
                width: 20%;
                padding-right: 10%;
                white-space: nowrap; 
            }

            tr td:first-child {
              padding-top: 20px;
              padding-bottom: 20px;
            }
            
            tr td:last-child {
                border: 0;
            }

            td:nth-of-type(1):before { content: "Date"; padding-top: 10px; padding-bottom: 10px; }
            td:nth-of-type(2):before { content: "Place"; padding-top: 10px; padding-bottom: 10px; }
            td:nth-of-type(3):before { content: "Event"; padding-top: 10px; padding-bottom: 10px; }
            td:nth-of-type(4):before { content: "Carrier";  padding-top: 10px; padding-bottom: 10px; }

        }    

    </style>
</head>

<body>
    <div class="tracking-table">
        <table>
            <thead class="header">
                <tr>
                    <th>Date</th>
                    <th>Place</th>
                    <th>Event</th>
                    <th>Carrier</th>
                </tr>
            </thead>
            <tbody class="results-content">
                <tr>
                    <td>Thu, Aug 29, 2019</td>
                    <td>
                        <p class="ro flag">Constanta</p>
                    </td>
                    <td>
                        <p>Container will be gated out from this place</p>
                    </td>
                    <td>
                        <p class="ship icon">MAERSK KYRENIA</p>
                    </td>
                </tr>
                <tr>
                    <td>Thu, Aug 30, 2019</td>
                    <td>
                        <p class="ro flag">Constanta</p>
                    </td>
                    <td>
                        <p>Container will be discharged from the vessel</p>
                    </td>
                    <td>
                        <p class="ship icon">MAERSK KYRENIA</p>
                    </td>
                </tr>
                <tr>
                    <td>Fri, Aug 31, 2019</td>
                    <td>
                        <p class="eg flag">Port Said East</p>
                    </td>
                    <td>
                        <p>Container will be loaded on the vessel</p>
                    </td>
                    <td>
                        <p class="ship icon">MAERSK KYRENIA</p>
                    </td>
                </tr>
                <tr>
                    <td>Mon, Sept 01, 2019</td>
                    <td>
                        <p class="eg flag">Port Said East</p>
                    </td>
                    <td>
                        <p>Container will be discharged from the vessel</p>
                    </td>
                    <td>
                        <p class="ship icon">MAERSK HANGZHOU</p>
                    </td>
                </tr>
                <tr class="current">
                    <td>Tue, Sept 02, 2019</td>
                    <td>
                        <p class="sg flag">Singapore</p>
                    </td>
                    <td>
                        <p>Container is loaded on the vessel</p>
                    </td>
                    <td>
                        <p class="ship icon">MAERSK HANGZHOU</p>
                    </td>
                </tr>
                <tr>
                    <td>Wed, Sept 03, 2019</td>
                    <td>
                        <p class="sg flag">Singapore</p>
                    </td>
                    <td>
                        <p>Container was discharged from the vessel</p>
                    </td>
                    <td>
                        <p class="ship icon">MARIVIA</p>
                    </td>
                </tr>
                <tr>
                    <td>Sun, Sept 04, 2019</td>
                    <td>
                        <p class="vn flag">Qui Nhon</p>
                    </td>
                    <td>
                        <p>Container was loaded on the vessel</p>
                    </td>
                    <td>
                        <p class="ship icon">MARIVIA</p>
                    </td>
                </tr>
                <tr>
                    <td>Tue, Sept 05, 2019</td>
                    <td>
                        <p class="vn flag">
                            Qui Nhon
                        </p>
                    </td>
                    <td>
                        <p>Container was located at this place</p>
                    </td>
                    <td>
                        <p class="ship icon">MARIVIA</p>
                    </td>
                </tr>
                <tr>
                    <td>Tue, Sept 06, 2019</td>
                    <td>
                        <p class="vn flag">Qui Nhon</p>
                    </td>
                    <td>
                        <p>Empty container was released to shipper for stuffing</p>
                    </td>
                    <td>
                        <p class="ship icon">MARIVIA</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>        
    <p id="result"></p>
    <script type="text/javascript">

        //Highlight the next date of the transportation
        var y = document.querySelectorAll("tr");
        var tableDate = "";
        var dateColumn = [];
        var date = new Date();
        date = date.toISOString();
        for (var i = 1; i < y.length; i++) {
            cellDate = y[i].firstElementChild.innerHTML;
            cellDate = cellDate.slice(5);
            x = getDateFormat(cellDate);
            if (date < x) {
                y[i].style.background = "#5C98CC";
                y[i].style.color = "#ffffff";
                break;
            }
        }

        //Convert string date to ISO format
        function getDateFormat(cellDate) {
            var monthList = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var cellYear = cellDate.slice(cellDate.length - 4);
            cellDate = cellDate.replace(", " + cellYear, "");
            var cellDay = cellDate.slice(cellDate.length - 2);
            var cellMonth = cellDate.replace(" " + cellDay, "");
            for (var j = 0; j < monthList.length; j++) {
                var pos = monthList[j].indexOf(cellMonth);
                if (pos != -1) {
                    cellMonth = j + 1;
                    if (cellMonth > 0 && cellMonth < 10) {
                        cellMonth = "0" + cellMonth;
                    }
                }
            }
            var dateResult = cellYear + "-" + cellMonth + "-" + cellDay + " 0:00:00";
            return dateResult;
        }
        
    </script>
</body>

</html>