<!DOCTYPE html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="robots" content="noindex">
    <meta name="description" content="">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Lato|Varela+Round&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="theme/css/create-route.css">
</head>
<body>
    <div class="port-form">
        <form id="port-info">
            <label>Cordinate List: </label>
            <textarea id="cor-list"></textarea>
            <input type="submit" value="Add">
            <label>ID List: </label>
            <textarea id="id-list"></textarea>
            <input type="submit" value="Add">
            <label>Port Name: </label>
            <input type="text" name="pname">
            <div id="input-port">
                <div id="box-1">
                    <label>Port Code: </label>
                    <input type="text" name="pcode">
                </div>
                <div id="box-2">
                    <label>Cordinate ID: </label>
                    <input type="text" name="cid">
                </div>
            </div>    
            <input type="submit" value="Add">
        </form>
    </div>  
    <div id="map">
        <textarea id="json_polyline" style='height:5%; width:100%'></textarea>
    </div>
</body>
<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp">
   </script> -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNQtfi4C8QpQHBwTMqmNOufW-lPrPn-i8&callback=initMap"></script>
<script>
   function initialize() {
       var mapOptions = {
           zoom: 3,
           center: new google.maps.LatLng(0, 60),
       }
       var map = new google.maps.Map(document.getElementById('map'), mapOptions);
       var polyOptions = {
           geodesic: false,
           strokeColor: '#00FF00',
           strokeOpacity: 1.0,
           strokeWeight: 2,
           editable: true,
       }
       var poly = new google.maps.Polyline(polyOptions);
       poly.setMap(map);
   
       //Call draw area here
       drawArea();
       markArea();
   
       google.maps.event.addListener(map, "click", function(event) {
           var path = poly.getPath();
           path.push(event.latLng);
   
           var coordinates_poly = poly.getPath().getArray();
           var newCoordinates_poly = [];
           for (var i = 0; i < coordinates_poly.length; i++) {
               lat_poly = coordinates_poly[i].lat();
               lng_poly = coordinates_poly[i].lng();
   
               latlng_poly = [lat_poly, lng_poly];
               newCoordinates_poly.push(latlng_poly);
           }
           var str_coordinates_poly = JSON.stringify(newCoordinates_poly);
           var json_poly = "{\"ToaDo\":" + str_coordinates_poly + "}";
   
           document.getElementById('json_polyline').value = json_poly;
   
       });
   
       function drawArea() {
           for (let i = -180; i < 180; i += 5) {
               let longitude = [
                   { lat: 85, lng: i },
                   { lat: -85, lng: i }
               ]
   
               let flightPath = new google.maps.Polyline({
                   path: longitude,
                   strokeColor: '#351fdb',
                   strokeOpacity: 0.5,
                   strokeWeight: 2
               });
   
               flightPath.setMap(map);
           }
   
           for (let i = -80; i < 90; i += 5) {
               let latitude = [
                   { lat: i, lng: -180 },
                   { lat: i, lng: 0 },
                   { lat: i, lng: 180 }
               ]
               let flightPath = new google.maps.Polyline({
                   path: latitude,
                   strokeColor: '#a11cad',
                   strokeOpacity: 0.5,
                   strokeWeight: 2
               });
   
               flightPath.setMap(map);
           }
       }
   
       function markArea() {
           let areaLat, areaLng;
           for (let lng = -179; lng < 180; lng += 5) {
            areaLng = Math.ceil((180-lng)/5);
               for (let lat = -81; lat < 80; lat += 5) {
                areaLat = Math.ceil((90-lat)/5);
   
                   let latLng = { lat: lat, lng: lng };
                   let image = {
                       url: 'info.png',
                       anchor: new google.maps.Point(5, 5),
                       scaledSize: new google.maps.Size(10, 10)
                   };
   
                   let harborLocation = new google.maps.Marker({
                       position: latLng,
                       title: `AreaLat: ${areaLat} \nAreaLong: ${areaLng}`,
                       icon: image,
                       map: map
                   });
               }
           }
       }
   }
   
   google.maps.event.addDomListener(window, 'load', initialize);
</script>