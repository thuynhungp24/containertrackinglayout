$("document").ready(function(){
	$(".collapse-side-panel img").hide();
	// If the table is visible
	if(!$(".tracking-table").is(":hidden")){ 
		$(".collapse-side-panel img").show()
	};	
	$("#searchBtn").click(function(){
		$(".collapse-side-panel img").show();
		$(".tracking-table").slideDown()
	});
	$("#cancelBtn").click(function(){
		$("#contentinfo").slideUp();
		$("input[name=Container]").val("");
		$("select").val("");
		$(".collapse-side-panel img").hide()
	});
	$("select").change(function(){
		if($(this).val() == "") $(this).addClass("empty");
    	else $(this).removeClass("empty")
	})
	$("select").change();
	// Toggle direction of hiding table in mobile view
    function tableHideContent(x){
    	// If width is smaller than 450px
        if(x.matches) {
        	$("#arrow").attr("src", "theme/img/arrow-down.png");
            $("#arrow").click(function(){
                    bottomCollapse = parseInt($("#contentinfo").outerHeight());
                    if (parseInt($("#contentinfo").css("top"), 10) == 0){   
	                    $("#contentinfo").animate({top: bottomCollapse});
	                    console.log(bottomCollapse);
	                    btnCollapse = bottomCollapse + 200;
	                    $("#arrow").animate({top: btnCollapse});
	                    $("#cancelBtn").animate({top: btnCollapse + 30});
	                    $("#arrow").attr("src", "theme/img/arrow-up.png");
                    } else {
		        		$("#contentinfo").animate({top: "0"});
		        		$("#arrow").animate({top: "200"});
		        		$("#cancelBtn").animate({top: "240"});
		        		$("#arrow").attr("src", "theme/img/arrow-down.png");
                    }
            })
        } else {
			$("#arrow").click(function(){
				var leftCollapse = parseInt($("#contentinfo").outerWidth());
				console.log(parseInt($("#contentinfo").outerWidth()));
		        if (parseInt($("#contentinfo").css('left'), 10) == 0){
		        	$("#contentinfo").animate({left: -leftCollapse});
		        	$(".collapse-side-panel img").animate({left: "0"});
					$("#arrow").attr("src", "theme/img/arrow-right.png");
		        } else {
		        	$("#contentinfo").animate({left: "0"});
		        	$(".collapse-side-panel img").animate({left: leftCollapse});
					$("#arrow").attr("src", "theme/img/arrow-left.png");
		        }
		    });
        }    
    };
    var mobileWidth = window.matchMedia("(max-width: 450px)");
    tableHideContent(mobileWidth);
    mobileWidth.addListener(tableHideContent);
});