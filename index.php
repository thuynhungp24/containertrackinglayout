<!DOCTYPE html >

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="robots" content="noindex">
    <meta name="description" content="">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="theme/js/custom.js"></script>
    <title>Container/HBL Tracking</title>
    <link rel="stylesheet" type="text/css" href="theme/css/style.css">
    <!--<link rel="stylesheet" type="text/css" href="maeu.css">-->
</head>

<body>
    <div id="logo">
        <img src="theme/img/logo.png" alt="logo" style="height:50px;">
    </div>
    <div class="loader-wrapper" id="loader-1">
        <div id="loader"> </div>
    </div>
    <div id="cover-div">
        <form id="form1" name="form1" method="post" action="#">
            <div class="containerSearch">
                <div class="rowSearch">
                    <div class="col-75">
                        <input type="text" placeholder="Container No." value="TTNU1270101" name="Container">
                        <select name="line">
                            <option value="" selected="selected">SEALINE...</option>
                            <option value="maersk">MAERSK</option>
                            <option value="msc">MSC</option>
                            <option value="cma-cgm">CMA-CGM</option>
                            <option value="hamburgsud">HAMBURG SUD</option>
                            <option value="hapag">HAPAG-LLOYD</option>
                            <option value="one">ONE</option>
                        </select>
                        <input id="searchBtn" type="submit" value="Search" onClick="javascript:showLoading()" /> 
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div id="content_loadport"><img src="theme/img/container_ship_loading-128.png" height="25" alt="">Load:Tue, June 04, 2019
        <br><strong>Qui Nhon</strong></div>
    <div id="content_destport"><img src="theme/img/dest-port-icon.png" height="25" alt="">Arrival:Thu, July 18, 2019
        <br><strong>Constanta</strong></div>
    <div id="content_shipBB"><strong>CARRIER</strong></br><img src="theme/img/ship.jpg" height="50" alt=""></div>
    
    <div id="map"></div>
    <div align="center"></div>
    <div class="collapse-side-panel">
        <img id="cancelBtn" src="theme/img/close.png">
        <img id="arrow" src="theme/img/arrow-left.png">
    </div>
    <div id="contentinfo">
        <iframe src="info-table.php" name="frame2" id="frame2" frameborder="0" marginwidth="" marginheight="" scrolling="auto" onload="" allowtransparency="false">
        </iframe>
    </div>
    <script>
        var x = document.getElementById("loader");
        x.style.display = "none";

        var map, popup_discharge, popup_load, popup_dest, popup_ship, Popup;

        /* Initializes the map and the custom popup. */
        function initMap() {
            definePopupClass();

            map = new google.maps.Map(document.getElementById('map'), {
                center: new google.maps.LatLng(0, 60),
                mapTypeControl: false,
                streetViewControl: false,
                zoom: 3
            });

            var polyOptions = {
            geodesic: false,
            strokeColor: '#00FF00',
            strokeOpacity: 1.0,
            strokeWeight: 2,
            editable: true,
            }

            var poly = new google.maps.Polyline(polyOptions);
            poly.setMap(map);

            popup_load = new Popup(
                new google.maps.LatLng(13.7829673, 109.2196634),
                document.getElementById('content_loadport'));
            popup_load.setMap(map);

            var marker = new google.maps.Marker({
                position: {
                    lat: 13.7829673,
                    lng: 109.2196634
                },
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 4.5,
                    fillColor: "#F00",
                    fillOpacity: 0.4,
                    strokeWeight: 0.4
                },

                map: map
            });

            popup_dest = new Popup(
                new google.maps.LatLng(44.1598013, 28.6348138),
                document.getElementById('content_destport'));
            popup_dest.setMap(map);

            var marker2 = new google.maps.Marker({
                position: {
                    lat: 44.1598013,
                    lng: 28.6348138
                },
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 4.5,
                    fillColor: "#F00",
                    fillOpacity: 0.4,
                    strokeWeight: 0.4
                },

                map: map
            });

            popup_ship = new Popup(
                new google.maps.LatLng(37.40106, 24.7273),
                document.getElementById('content_shipBB'));
            popup_ship.setMap(map);

            var markerImage = new google.maps.MarkerImage('theme/img/bluedot.png', null, // size
                null, // origin
                new google.maps.Point(8, 6), // anchor (move to center of marker)
                new google.maps.Size(15, 15) // scaled size (required for Retina display icon)
            );
            var marker3 = new google.maps.Marker({
                map: map,
                draggable: false,
                optimized: false,
                flat: true,
                visible: true,
                icon: markerImage,
                title: 'Your container might be here',
                animation: google.maps.Animation.DROP,
                // map default location set in Tehran location
                position: {
                    lat: 37.40106,
                    lng: 24.7273
                }
            });

        }

        /** Defines the Popup class. */
        function definePopupClass() {
            /**
             * A customized popup on the map.
             * @param {!google.maps.LatLng} position
             * @param {!Element} content
             * @constructor
             * @extends {google.maps.OverlayView}
             */
            Popup = function(position, content) {
                this.position = position;

                content.classList.add('popup-bubble-content');

                var pixelOffset = document.createElement('div');
                pixelOffset.classList.add('popup-bubble-anchor');
                pixelOffset.appendChild(content);

                this.anchor = document.createElement('div');
                this.anchor.classList.add('popup-tip-anchor');
                this.anchor.appendChild(pixelOffset);

                // Optionally stop clicks, etc., from bubbling up to the map.
                this.stopEventPropagation();
            };
            // NOTE: google.maps.OverlayView is only defined once the Maps API has
            // loaded. That is why Popup is defined inside initMap().
            Popup.prototype = Object.create(google.maps.OverlayView.prototype);

            /** Called when the popup is added to the map. */
            Popup.prototype.onAdd = function() {
                this.getPanes().floatPane.appendChild(this.anchor);
            };

            /** Called when the popup is removed from the map. */
            Popup.prototype.onRemove = function() {
                if (this.anchor.parentElement) {
                    this.anchor.parentElement.removeChild(this.anchor);
                }
            };

            /** Called when the popup needs to draw itself. */
            Popup.prototype.draw = function() {
                var divPosition = this.getProjection().fromLatLngToDivPixel(this.position);
                // Hide the popup when it is far out of view.
                var display =
                    Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ?
                    'block' :
                    'none';

                if (display === 'block') {
                    this.anchor.style.left = divPosition.x + 'px';
                    this.anchor.style.top = divPosition.y + 'px';
                }
                if (this.anchor.style.display !== display) {
                    this.anchor.style.display = display;
                }
            };

            /** Stops clicks/drags from bubbling up to the map. */
            Popup.prototype.stopEventPropagation = function() {
                var anchor = this.anchor;
                anchor.style.cursor = 'auto';

                ['click', 'dblclick', 'contextmenu', 'wheel', 'mousedown', 'touchstart',
                    'pointerdown'
                ]
                .forEach(function(event) {
                    anchor.addEventListener(event, function(e) {
                        e.stopPropagation();
                    });
                });
            };
        }

        function showLoading() {
            var x = document.getElementById("loader");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCNQtfi4C8QpQHBwTMqmNOufW-lPrPn-i8&callback=initMap&maptype=roadmap"></script>
</body>

</html>